var jQuery , $;
$ = require("jquery");
window.$ = window.jQuery = jQuery = $;
require('./modules/browserUtils');
require('jquery.transit');


//
import Scrolly from './modules/scrolly';
window.Scrolly = Scrolly;
$(window).on('load', function() {
  if($('[data-module="medalsArvin"]').length > 0){
    Scrolly.do($('.sub-modals-container')[0],100);
  }
});


import Zoommer from './modules/zoommer';
window.Zoommer = Zoommer;
window.Zoommer.attach();


require('slick-carousel');
// $(window).on('load', function() {
//   console.log($('.variable-width'));
//   $('.variable-width').slick({
//     // dots: true,
//     infinite: true,
//     speed: 500,
//     slidesToShow: 1,
//     centerMode: true,
//     variableWidth: true
//   });
// });
$(document).ready(function(){
  // $('.carousel').slick({
  //   slidesToShow: 3,
  //   dots:true,
  //   centerMode: true,
  // });
  // $('.variable-width').slick({
  //   dots: true,
  //   infinite: true,
  //   speed: 300,
  //   slidesToShow: 1,
  //   centerMode: true,
  //   variableWidth: true
  // });
  $('.center').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });
});



import Navbar from './modules/navbar';
import MedalsSlider from './modules/medals-slider';
import PopUp from './modules/popup';
import Header from './modules/header';
import Map from './modules/map';
import MedalsArvin from './modules/medals-arvin';

import PopupZoommer from './modules/popup-zoommer';
window.PopupZoommer = PopupZoommer;

import PopupSlider from './modules/popup-slider';
window.PopupSlider = PopupSlider;


Navbar.init();
MedalsSlider.init();
PopUp.init();
PopUp.attach();
Header.init();
Map.init();
MedalsArvin.init();
