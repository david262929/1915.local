window.is_mob = function () {
  if( navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
  ) {
    return true;
  }
  else {
    return false;
  }
}


window.isTouchDevice = function (){
  return ((navigator.maxTouchPoints > 0) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
}



let $win = $(window),
    $body = $(document.body);
$win.on('resize',resetState);


// Listen to resize event to update media-query state.
function resetState() {
  if(isTouchDevice()){
    $body.addClass('isTouch');
  }else{
    $body.removeClass('isTouch');
  }


  var w = $win.get(0).innerWidth;
  if (w < 868) {
    window.deviceSize = 'mobile';
  } else if (w >= 868 && w < 1024) {
    window.deviceSize = 'tabletportrait';
  } else if (w >= 1024 && w < 1080) {
    window.deviceSize = 'tabletlandscape';
  }else if (w >= 1080 && w < 1280) {
    window.deviceSize = 'nonedesktop';
  } else {
    window.deviceSize  = 'desktop';
  }
}
resetState();

// attach Cubic-bezier initator
(function(factory) {
  if (typeof exports === "object") {
    factory(require("jquery"));
  } else if (typeof define === "function" && define.amd) {
    define(["jquery"], factory);
  } else {
    factory(jQuery);
  }
}(function($) {
  $.extend({ bez: function(encodedFuncName, coOrdArray) {
      if ($.isArray(encodedFuncName)) {
        coOrdArray = encodedFuncName;
        encodedFuncName = 'bez_' + coOrdArray.join('_').replace(/\./g, 'p');
      }
      if (typeof $.easing[encodedFuncName] !== "function") {
        var polyBez = function(p1, p2) {
          var A = [null, null], B = [null, null], C = [null, null],
            bezCoOrd = function(t, ax) {
              C[ax] = 3 * p1[ax], B[ax] = 3 * (p2[ax] - p1[ax]) - C[ax], A[ax] = 1 - C[ax] - B[ax];
              return t * (C[ax] + t * (B[ax] + t * A[ax]));
            },
            xDeriv = function(t) {
              return C[0] + t * (2 * B[0] + 3 * A[0] * t);
            },
            xForT = function(t) {
              var x = t, i = 0, z;
              while (++i < 14) {
                z = bezCoOrd(x, 0) - t;
                if (Math.abs(z) < 1e-3) break;
                x -= z / xDeriv(x);
              }
              return x;
            };
          return function(t) {
            return bezCoOrd(xForT(t), 1);
          }
        };
        $.easing[encodedFuncName] = function(x, t, b, c, d) {
          return c * polyBez([coOrdArray[0], coOrdArray[1]], [coOrdArray[2], coOrdArray[3]])(t/d) + b;
        }
      }
      return encodedFuncName;
    }});
}));
