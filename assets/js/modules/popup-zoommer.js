let initedPopupZoomers = [];

function PopupZoommer($popupZoommer = null) {
  let _self = this;
  if($popupZoommer.length == 0){
    return;
  }
  _self.$popupZoommer = $popupZoommer;


  _self.init = function() {

    _self.popupZoommerImgLoader(_self.$popupZoommer.attr('data-popup-zoommer-src'),_self.$popupZoommer.attr('data-popup-zoommer-class')).then(function () {
      if(window.isTouchDevice()){
        _self.$popupZoommer.css({'overflow': 'auto'})
        return;
      }
      _self.attachEvents();
    }).catch(function () {

    });

  };

  _self.attachEvents = function() {
    _self.$zoomElement.on("mousemove", _self.startPopupZoommer);
  };


  _self.startPopupZoommer = function (event = null){
    if(window.isTouchDevice()){ return; }
    // console.log(event,this,!(this instanceof popupZoommer));
    let $positionByPercent = event && !(this instanceof PopupZoommer) ? _self.getMousePositionOnElement(event, this) : {top : 0, left : 0};
    // console.log($positionByPercent);return;
    let zoomElementNewPosition = {};

    zoomElementNewPosition.top = ( _self.$popupZoommer.innerHeight() - _self.$zoomElement.innerHeight() ) * ( $positionByPercent.top / 100 ),
      zoomElementNewPosition.left = ( _self.$popupZoommer.innerWidth() - _self.$zoomElement.innerWidth() ) * ( $positionByPercent.left / 100 );
    _self.$zoomElement.css(zoomElementNewPosition);
  };

  _self.getMousePositionOnElement = function (event, thisElement) {
    thisElement = thisElement.closest('.popup-zoommer');
    let $popupZoommer = _self.$popupZoommer,
      x = event.pageX - window.scrollX - thisElement.getBoundingClientRect().left,
      y = event.pageY - window.scrollY - thisElement.getBoundingClientRect().top, //
      w = $popupZoommer.innerWidth(),
      h = $popupZoommer.innerHeight();
    return { top : y / h * 1e2, left : x / w * 1e2}
  };

  _self.popupZoommerImgLoader = function(src = '', className = ''){
    return new Promise(function (resolve) {
      _self.$zoomElement = $('<img src="' + src + '" class="' + className + '">');
      // _self.restyleZoomElement();
      _self.$zoomElement.on('load', function () {
        _self.$zoomElement.appendTo(_self.$popupZoommer);
        resolve();
      });
    })
  };

  _self.init();
}

function _do($element = null) {
  if($element.length > 0 && !initedPopupZoomers.includes($element[0])){
    initedPopupZoomers.push($element[0]);
    new PopupZoommer($($element[0]));
  }
}

export default {
  do : _do,
};