function PopUp($popupOpener = null, onOpenedCallback = null) {
  let _self = this;
  if($popupOpener.length == 0){
    return;
  }
  _self.onOpenedCallback = (typeof  onOpenedCallback != 'function') ? function () {} : onOpenedCallback;

  _self.attachedImages = false;
  _self.popupOpener = $popupOpener;

  _self.attachImages = function (){
    if(_self.attachedImages) return;
    _self.attachedImages = true;
    console.log('attaching Images');
  }

  _self.attachEvents = function () {

    _self.popupOpener.on('click',function () {
      let $popup = $(`${_self.popupOpener.attr('data-target-popup')}`);
      if($popup.length <= 0) return;
      _self.onOpenedCallback();
      _self.attachImages();

      $('html, body').css({"overflow-y":"hidden"});

      let $popupSlider = $popup.find('.popup-slider');
      if($popupSlider.length > 0 ){
        PopupSlider.add($popupSlider);
      }


      window.$activePopUp = $popup;
      $popup.attr('data-is-opened', 'true').closest('.sub-text-container').css('z-index','99999999');
    });


  };

  _self.attachEvents();
}

function _init() {
  $.each($('[data-target-popup$="-popup"]'),function( key, value ) {
    new PopUp($(value));
  })
}

function _addPopUp($el, onOpenedCallback = null) {
  if($el.length <= 0) return;
  new PopUp($elm, onOpenedCallback);
}

function _attach() {
  window.$(document).keyup(function(e) {
    if (e.key === "Escape" && typeof $activePopUp != 'undefined' && $activePopUp != null && $activePopUp.length > 0) {

      let $preview, $confirmer;

      if( ($confirmer = $activePopUp.find('.popup_content .about .sub-confirm')).attr('data-opened-confirmer') == 'true'){
        $confirmer.attr('data-opened-confirmer', false);
        return;
      }
      if(window.deviceSize == 'mobile' && ($preview = $activePopUp.find('.preview')).attr('data-opened-preview') == 'true'){
        $preview.attr('data-opened-preview',false);
        return;
      }
      $('html, body').css({"overflow-y":"auto"});
      $activePopUp.attr('data-is-opened', 'false').closest('.sub-text-container').css('z-index','99997');
      $activePopUp = null;
    }
  });


  let $popups = $('.popup');
  $popups.on('click', '.close-popup , .close-area', function () {
    let $popup = $(this).closest('.popup'), $preview, $confirmer;

    if( ($confirmer = $activePopUp.find('.popup_content .about .sub-confirm')).attr('data-opened-confirmer') == 'true'){
      $confirmer.attr('data-opened-confirmer', false);
      return;
    }

    if(window.deviceSize == 'mobile' && ($preview = $popup.find('.preview')).attr('data-opened-preview') == 'true'){
      $preview.attr('data-opened-preview',false);
      return
    }
    $('html, body').css({"overflow-y":"auto"});
    $popup.attr('data-is-opened', 'false').closest('.sub-text-container').css('z-index','99997');
  });

  $.each($popups.find('.popup_content'), function (index, value) {
    $(value).css('transition', '.5s cubic-bezier(.165,.84,.44,1)')
  })

  $popups.find('.confirm').on('click', function () {
    $(this).closest('.popup_content .about').find('.sub-confirm').attr('data-opened-confirmer',true);
  });



  $popups.find('.sub-confirm').on('click','.close-confirm , .apply-confirm', function(){
    $(this).closest('.about .sub-confirm').attr('data-opened-confirmer',false);
  });

  $popups.find('.preview_opener').on('click', function () {
    $(this).closest('.sub_popup_content').find('.preview').attr('data-opened-preview',true);
  });
}

export default {
  attach : _attach,
  init : _init,
  addPopUp  : _addPopUp,
};