function Navbar() {
  let _self = this;
   _self.$el = $('[data-module="nav-bar"]');
  if(_self.$el.length <= 0){
    return {};
  }
  _self.$navigations = _self.$el.find('.navigations');
  let $screenHeight = $(window).height();
  _self.inProccess = false;


  _self.attachEvents = function () {

    _self.doLayout();
    // console.log(_self.oldColorOfToggler);

    _self.$el.find('.toggler-menu').click(function() {
      if(_self.inProccess) return;

      _self.inProccess = true;
      if (_self.$el.attr('data-is-collapsed') == 'true') {
        setTimeout(function () {
          _self.inProccess = false;
        },1800);
        _self.$el.attr('data-is-collapsed', false).addClass('maxZindex');
        return;
      }
      _self.$el.attr('data-is-collapsed', true).removeClass('maxZindex');
      setTimeout(function () {
        _self.inProccess = false;
      },1200);
    });

    window.$(document).keyup(function(e) {
      if (e.key === "Escape" && _self.$el.attr('data-is-collapsed') == 'true') {
        _self.$el.attr('data-set-color', _self.oldColorOfToggler);
        _self.$el.attr('data-is-collapsed', false);
        _self.$navigations.animate({
          top: "-100%"
        }, 500);
      }
    });
  };


  _self.doLayout = function () {

  };


  _self.attachEvents();
}


function _init(){
  new Navbar();
}

export default {
  init: _init
}