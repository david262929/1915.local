function isCorrectModuleHash(moduleHash = null) {
  if(typeof moduleHash != 'string' || moduleHash == '' || moduleHash == null){
    return false;
  }

    moduleHash = moduleHash.replace("#", ""); // remove default HashCode
  if(moduleHash == ''){
    return false;
  }

  return moduleHash;
}

function _isMyModuleHash(moduleHash = null, toReturnHash = false) {
  let result = isCorrectModuleHash(moduleHash);
  return  result ? ( toReturnHash ? result : !!(document.location.hash.indexOf('inspiration') > 0) ) : false;
}

function _getCode(moduleHash = null, myElemsCount = 1) {
  myElemsCount = isNaN(parseInt(myElemsCount)) ? 1 : parseInt(myElemsCount);
  myElemsCount = myElemsCount < 1 ? 1 : myElemsCount;

  moduleHash = _isMyModuleHash(moduleHash, true);
  if(!moduleHash){
    return false;
  }

  moduleHash = moduleHash.replace("#", ""); // remove default HashCode
  if(moduleHash == ''){
    return false;
  }

  let index = document.location.hash;
  index = index.slice(index.indexOf(moduleHash), index.length);
  index = parseInt(index.split(":")[1]);

  let newIndex = (isNaN(index) || index <= 0) ? 1 : ((index > myElemsCount) ? myElemsCount : index);
  if(newIndex != index){
    _setCode(moduleHash, newIndex);
  }
  return newIndex;
}

function _setCode(moduleHash = null, newIndex = null) {
  moduleHash = isCorrectModuleHash(moduleHash);

  if(!moduleHash || newIndex == null){
    return false;
  }

  let currentHash = document.location.hash;
  currentHash = currentHash.slice(currentHash.indexOf(moduleHash), currentHash.length);
  document.location.hash = currentHash.replace(currentHash, moduleHash + ':' + newIndex);

  return true;
}

// function _getCurrentHash() {
//
// }

export default {
  isMyModuleHash : _isMyModuleHash,
  getCode        : _getCode,
  setCode        : _setCode,
};