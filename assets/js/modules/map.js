function Map($map = null) {
  let _self = this;
  if($map.length == 0){
    return;
  }

  _self.init = function() {
    _self.attachEvents();
  };
  _self.attachEvents = function() {

    let $mapPopup = $map.find('.map-popup')
    $map.find('.map').on('click', function (event) {
      window.$activePopUp = $mapPopup;
      $('html, body').css({"overflow-y":"hidden"});
      $mapPopup.attr('data-is-opened', true).closest('[data-module="map"]').css('z-index','99999999');
      setTimeout(function () {
        Zoommer.do($mapPopup.find('.img-container img') );
      },500);
    });

    // $map = $map.find('.map-popup');

    window.addEventListener('resize', _self.mobileScrollCorrector);
    _self.mobileScrollCorrector();
  };

  _self.mobileScrollCorrector = function(){
    if(!window.isTouchDevice()){ return; }
    $map.find('.mobile-map').scrollLeft( ($map.find('img.static-mobile').innerWidth() - $map.innerWidth()) / 2 );
  };


  _self.init();
}

function _init() {
  $.each($('[data-module="map"]'),function( key, value ) {
    new Map($(value));
  })

}

export default {
  init : _init,
};