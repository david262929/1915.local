let initedPopupSliders = [];

function PopupSlider($popupSlider = null, onOpenedCallback = null) {
  let _self = this;
  if($popupSlider.length == 0 && $popupSlider.hasClass('popup-slider')){
    return;
  }
  _self.$popupSlider = $popupSlider;

  _self.$popupSlides = $popupSlider.find('.popup-slide');

  if(_self.$popupSlides.length <= 1){
    return;
  }
  _self.$controls = $popupSlider.find('.controls');

  if(_self.$popupSlides.length == 0){
    return;
  }

  _self.attachEvents = function () {
    _self.popupTranstion = 0;
    $.each(_self.$popupSlides, function ( key, value ){
      $(value).css('transform','translate('+_self.popupTranstion + '% ,0)')
    });
    let $popupZommer = _self.$active = _self.$popupSlides.first();
    if($popupZommer.hasClass('popup-zoommer')){
      PopupZoommer.do($popupZommer);
    }

    _self.$controls.find('.to-left').on('click', function () {
      let $prev = _self.$active.prev('.popup-slide');
      if($prev.length > 0 && $prev != _self.$active.parent().last('.popup-slide')){
        if($prev.hasClass('popup-zoommer')){
          PopupZoommer.do($prev);
        }
        _self.$active = $prev;
        _self.popupTranstion += 100;
        $.each(_self.$popupSlides, function ( key, value ){
          $(value).css('transform','translate('+_self.popupTranstion + '% ,0)')
        });
      }
    });

    _self.$controls.find('.to-right').on('click', function () {
      let $next = _self.$active.next('.popup-slide');
      if($next.length > 0 && $next != _self.$active.parent().first('.popup-slide')){
        if($next.hasClass('popup-zoommer')){
          PopupZoommer.do($next);
        }
        _self.$active = $next;
        _self.popupTranstion -= 100;
        $.each(_self.$popupSlides, function ( key, value ){
          $(value).css('transform','translate('+_self.popupTranstion + '% ,0)')
        });
      }
    });


  };

  _self.attachEvents();
}

function _init() {
  // if($element.length > 0 && !initedPopupSliders.includes($element[0])){
  //   // initedPopupSliders.push($element[0]);
  //   // new PopupSlider($($element[0]));
  // }
}

export default {
  init : _init
};