function MedalsArvin($medalModule = null) {
  let _self = this;
  if($medalModule.length == 0){
    return;
  }
  let $win = $(window);
  let $subMedalsContainer = $medalModule.find('.sub-modals-container');
  _self.medals = $subMedalsContainer.find('.medal');
  _self.activeMedalHead = $subMedalsContainer.find('.medal .text-container .fixator[data-is-active="true"]');
  if(_self.activeMedalHead.length <= 0){
    _self.activeMedalHead = $subMedalsContainer.find('.medal:first-child .text-container .fixator');
  }

  Zoommer.do(_self.activeMedalHead.closest('.medal').find('.medal-container img:first-of-type'));
  _self.init = function() {
    _self.attachEvents();
  };


  _self.attachEvents = function() {
    // $.each($medalModule.find('.medal-container'), function (key, value) {
    //   let $this = $(value);
    //   let $otherHand = $this.find('.medal-on-other-hand');
    //   $this.on('mouseenter', function () {
    //     let src = $this.attr('data-both-img-src');
    //     if($this.attr('data-loaded-image') == 'true' || src == ''){ return };
    //     let $img = $('<img src="' + src + '">');
    //     $img.on('load', function () {
    //       setTimeout(function () {
    //         $this.attr('data-loaded-image', true);
    //         setTimeout(function () {
    //           $otherHand.removeClass('loading');
    //         }, 100)
    //         $otherHand.css({"background-image": "url(" + src + ")"});
    //       }, 1000)
    //     });
    //   })
    // });


    // let lastScrollTop = 0;
    $subMedalsContainer.scroll(function(event){
      let medalsCenter = $subMedalsContainer.outerHeight(true) / 2 ;
      // $('#line').css('top',medalsCenter+'px');
      $.each(_self.medals , function (key, value){
        let $this = $(value);

        let offsetOfParent = $this.offset().top - $this.parent().offset().top;

        if( parseFloat(offsetOfParent) < medalsCenter && medalsCenter <  parseFloat(offsetOfParent + $this.outerHeight(true)) ) {
          // console.log(parseFloat($this.attr('data-top')) < medalsCenter , medalsCenter , medalsCenter <  parseFloat($this.attr('data-bottom')) );
          _self.activeMedalHead.attr('data-is-active',false);
          _self.activeMedalHead = $this.find('.text-container .fixator');
          _self.activeMedalHead.attr('data-is-active',true);
          Zoommer.do(_self.activeMedalHead.closest('.medal').find('.medal-container img:first-of-type'));
          return false;
        }
      });
    });

  };

  _self.init();
}

function _init() {
  new MedalsArvin($('[data-module="medalsArvin"]'));
}

export default {
  init : _init,
};