let initedPopupSliders = [];

function PopupSlider($popupSlider = null, onOpenedCallback = null) {
  let _self = this;
  if($popupSlider.length == 0 && $popupSlider.hasClass('popup-slider')){
    return;
  }
  _self.inProccess = false;
  _self.$popupSlider = $popupSlider;

  _self.$popupSlides = $popupSlider.find('.popup-slide');

  if(_self.$popupSlides.length <= 1){
    return;
  }
  _self.$controls = $popupSlider.find('.controls');

  if(_self.$popupSlides.length == 0){
    return;
  }

  _self.attachEvents = function () {
    _self.popupTranstion = -100;
    $.each(_self.$popupSlides, function ( key, value ){
      $(value).css('transform','translate('+_self.popupTranstion + '% ,0)')
    });
    let $popupZommer = _self.$active = _self.$popupSlides.eq(1);
    if($popupZommer.hasClass('popup-zoommer')){
      PopupZoommer.do($popupZommer);
    }

    _self.$controls.find('.to-left').on('click', function () {
      if(_self.inProccess) return;

      _self.inProccess = true;
      let $prev = _self.$active.prev('.popup-slide');
      if($prev.hasClass('popup-zoommer')){
        PopupZoommer.do($prev);
      }
      _self.popupTranstion += 100;
      let counter = 0;

      _self.$active = $prev;
      _self.$popupSlides.transition({'transform':'translate('+ _self.popupTranstion + '% ,0%)', complete : function () {
          counter++;
          if(counter == _self.$popupSlides.length){
            _self.inProccess = false;
            _self.popupTranstion -= 100;
            _self.$popupSlider.find('.popup-slides').prepend($popupSlider.find('.popup-slide:last-of-type'))
            _self.$popupSlides.css('transform','translate('+ _self.popupTranstion + '% ,0%)');
          }
        }},500,'easeOutCubic');
    });

    _self.$controls.find('.to-right').on('click', function () {
      if(_self.inProccess) return;

      _self.inProccess = true;
      let $next = _self.$active.next('.popup-slide');
      if($next.length == 0){
        $next = $popupSlider.find('.popup-slide:first-of-type');
        if($next.hasClass('popup-zoommer')){
          PopupZoommer.do($next);
        }
        let counter = 0;
        _self.popupTranstion += 100;
        _self.$popupSlider.find('.popup-slides').append($next);
        _self.$active = $next;
        _self.$popupSlides.css('transform','translate('+ _self.popupTranstion + '% ,0%)');

        _self.popupTranstion -= 100;
        _self.$popupSlides.transition({'transform':'translate('+ _self.popupTranstion + '% ,0%)', complete : function () {
            counter++;
            if(counter == _self.$popupSlides.length) {
              _self.inProccess = false;
            }
          }},500,'easeOutCubic');
        return;
      }
      if($next.hasClass('popup-zoommer')){
        PopupZoommer.do($next);
      }
      _self.popupTranstion -= 100;
      let counter = 0;

      _self.$active = $next;
      _self.$popupSlides.transition({'transform':'translate('+ _self.popupTranstion + '% ,0%)' , complete : function() {
        counter++;
        if(counter == _self.$popupSlides.length){
          _self.popupTranstion += 100;
          _self.$popupSlider.find('.popup-slides').append($popupSlider.find('.popup-slide:first-of-type'))
          _self.$popupSlides.css('transform','translate('+ _self.popupTranstion + '% ,0%)');
        }
      }},500,'easeOutCubic');
    });


  };

  _self.attachEvents();
}

function _add($element) {
  if($element.length > 0 && !initedPopupSliders.includes($element[0])){
    initedPopupSliders.push($element[0]);
    new PopupSlider($($element[0]));
  }
}

export default {
  add : _add,
};