function Header() {
  let _self = this;

  _self.header = $('[data-module="header"]');
  if(_self.header.length == 0){
    return {};
  }
  let $castleBlock = _self.header.find('.header-castle-block'),
      $win = $(window);

  _self.doLayout = function () {
    if($win.scrollTop() >= screen.height * 0.1){
      $castleBlock.addClass("notPinned");
      return;
    }
    $castleBlock.removeClass("notPinned");
  };

  window.addEventListener('scroll', _self.doLayout);
  _self.doLayout();
}

function _init() {
  new Header();
}

export default {
  init : _init,
};