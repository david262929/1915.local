<?php
ini_set('display_errors', 1);
define('IS_LOCAL', true);
define('IS_MY_SERVER', false);

$link = "http" . ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "s" : null ) . "://" . $_SERVER['HTTP_HOST'];
if(IS_LOCAL){
	$link .= '/1915.local';
}
if(IS_MY_SERVER){
    $link .= '/sites/1915.life';
}
define('HOME_URL', $link);
define('CURRENT_URL', $link . $_SERVER['REQUEST_URI']);

$defaults = [
    'lang' => 'en',
    'view' => 'home',
];

$cookieName = 'default1915LifeLanguage';

$availableLangs = ['en','hy','ru'];
$availableViews = ['home','book','about','order','test','medals','contact'];

if(empty($_GET['rout'])){
    $settingFromCookie = $_COOKIE[$cookieName] ?? null;
    $settings = $defaults;
    if(!empty($settingFromCookie)) {
        $settingFromCookie = unserialize($settingFromCookie, ["allowed_classes" => false]) ?? null;
        $settings['lang'] = $settingFromCookie['lang'] ?? $defaults['lang'];
    }
//    var_dump([$settings['lang'],(!in_array($settings['lang'], $availableLangs)) ? $defaults['lang'] : $settings['lang'] ] ); echo "<br>";

    define('LANG', (!in_array($settings['lang'], $availableLangs)) ? $defaults['lang'] : $settings['lang'] );
    define('VIEW', $defaults['view'] );
}else{

	$routes = explode('/', $_GET['rout']);

	if(in_array($routes[0], $availableLangs)){
		define('LANG', $routes[0]);
	}

	if(!defined('LANG')){
        define('LANG', $defaults['lang']);
        define('VIEW', '404');
	}

	if(!defined('VIEW')){
		if( empty($routes[1]) ){
			define('VIEW', $defaults['view']);
		} else if(in_array($routes[1], $availableViews)){
			define('VIEW', $routes[1]);
		}else{
			define('VIEW', '404');
		}
	}
}
$asdf  = setcookie($cookieName, serialize(['lang'=>LANG]), time() + (86400 * 31)); // 86400 = 1 day

$view = VIEW . '.php';
if( !file_exists($view) ){
	header("Location: " . HOME_URL . "/404.php");die();
}

define('ABS_PATH', true);

if(VIEW != '404'){
	$json = file_get_contents(HOME_URL . '/langs/' . LANG . '.json');
    $GLOBALS['pageData'] = json_decode($json,true);
    if(empty($GLOBALS['pageData'])){
        $view = '404.php';
    }
}

$ass = true;
include $view;
