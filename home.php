<?php
if(!defined('ABS_PATH') || empty($pageData)){ header("Location: 404.php");die();}
?>

<!DOCTYPE html>
<html lang="<?= LANG ?>" xml:lang="<?= LANG ?>"  xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<!--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
    <meta charset="UTF-8">
    <title>1915.life</title>
    <link rel="icon" href="<?= HOME_URL ?>/assets/img/favicon.png">
    <?php
    echo '<link rel="stylesheet" href="' . HOME_URL . '/assets/styles/css/main.css?v='.rand(111,12122).'">';
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1.0, user-scalable=no">
    <style>
        .nicescroll-rails{
            display: none!important;
        }
        *{
            user-select: none!important;
        }
    </style>
</head>
<body class="<?= LANG ?>">
<!--<div id="effect" class="ui-widget-content ui-corner-all" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0); width: 240px; transform:translate(100px,100px);  ">-->
<!--    <h3 class="ui-widget-header ui-corner-all">Animate</h3>-->
<!--    <p>-->
<!--        Etiam libero neque, luctus a, eleifend nec, semper at, lorem. Sed pede. Nulla lorem metus, adipiscing ut, luctus sed, hendrerit vitae, mi.-->
<!--    </p>-->
<!--</div>-->
<!--<div id="button">aaaaa</div>-->
<script>

    // var state = true;
    // $( "#button" ).on( "click", function() {
    //
    //   if ( state ) {
    //     $( "#effect" ).animate({
    //       backgroundColor: "#aa0000",
    //       transform:"translate(100px,100px)",
    //       color: "#fff",
    //       width: 500
    //     }, 1000 );
    //   } else {
    //     $( "#effect" ).animate({
    //       backgroundColor: "#fff",
    //       transform:"translate(0px,0px)",
    //       color: "#000",
    //       width: 240
    //     }, 1000 );
    //   }
    //   state = !state;
    // });

</script>
<?php
//die();
include_once ('components/navbar.php');
include_once ('components/header.php');
include_once ('components/map.php');
include_once ('components/medals-arvin.php');
include_once ('components/about-book.php');

?>


<?php
echo '<script src="' . HOME_URL . '/assets/js/build/app.min.js?v='.rand(111,12122).'"></script>';
?>
<script src=""></script>
</body>
</html>