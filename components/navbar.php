<div data-module="nav-bar" data-is-collapsed="start">
    <div class="logo-with-toggler">
        <div class="logo"><img src="<?= HOME_URL ?>/assets/img/logo_white.png"></div>
        <div class="toggler-menu">
            <div class="lines">
                <div class="line"></div>
                <div class="line"></div>
            </div>
        </div>
    </div>
    <div class="navigations">
        <div class="footer-info">
            <div class="info-main">
                <div class="design-studio-side">
                    <div class="logo distance-ten"><img src="<?= HOME_URL ?>/assets/img/logo_47design_black.png" alt=""></div>
                    <div class="studio-in-few-words distance-twenty">
                        <?= $pageData['nav-bar']['footer-info']['studio-in-few-words'] ?? null ?>
                    </div>
                    <div class="visit"><a href="<?= $pageData['nav-bar']['footer-info']['visit-link'] ?? null ?>"><?= $pageData['nav-bar']['footer-info']['visit'] ?? null ?></a></div>
                </div>
                <div class="contact-side">
                    <div class="sub-side distance-twenty">
                        <div class="sub-side-title distance-ten"><?= $pageData['nav-bar']['footer-info']['phone']['title'] ?? null ?></div>
                        <div class="desc">
                            <?php
                            $desc = $pageData['nav-bar']['footer-info']['phone']['desc'] ?? null;
                            if(!empty($desc)){
                                foreach ($desc as $phone){
                                    echo "<div class=\"phone distance-ten\">$phone</div>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="sub-side">
                        <div class="sub-side-title distance-ten"><?= $pageData['nav-bar']['footer-info']['email']['title'] ?? null ?></div>
                        <div class="desc">
                            <?php
                            $desc = $pageData['nav-bar']['footer-info']['email']['desc'] ?? null;
                            if(!empty($desc)){
                                foreach ($desc as $email){
                                    echo "<div class=\"email distance-ten\">$email</div>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-buttons">
            <div class="buttons-main">
                <?php
                $navButtons = $pageData['nav-bar']['nav-buttons'] ?? null;
                $home = HOME_URL;
                $view = VIEW;
                $lang = LANG;
                if(!empty($navButtons)){
                    foreach ($navButtons as $key => $navButton){
                        $active = VIEW == $navButton['view'] ? ' active ' : '';
                        echo "<div class=\"nav-button $active\"><div><a href=\" $home/$lang/{$navButton['view']}\">{$navButton['title']}</a></div></div>";
                    }
                }
                ?>
            </div>
            <div class="langs">
                <a class="lang <?= LANG == 'hy' ? 'active' : '' ?>" href="<?= HOME_URL ?>/hy/<?= VIEW ?>">Հայ</a>
                <a class="lang <?= LANG == 'en' ? 'active' : '' ?>" href="<?= HOME_URL ?>/en/<?= VIEW ?>">Eng</a>
                <a class="lang <?= LANG == 'ru' ? 'active' : '' ?>" href="<?= HOME_URL ?>/ru/<?= VIEW ?>">Рус</a>
                <a class="lang <?= HOME_URL ?>" href="#">Tür</a>
            </div>
            <div class="white-area">
                <div class="area"></div>
            </div>
        </div>
    </div>
</div>