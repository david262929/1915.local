<div data-module="header" data-color="white">
    <div class="header-text-block">
        <div class="text-block">
            <?php
            $lines = $pageData['header-text-block'] ?? null;
            if(!empty($lines)){
                foreach ($lines as $line){
                    echo "<div class=\"line {$line['class']}\">{$line['content']}</div>";
                }
            }
            ?>
        </div>
    </div>
    <div class="header-castle-block">
        <div class="sub-header-castle-block">
            <div class="castle-block">
                <div class="castle">
                    <img src="<?= HOME_URL ?>/assets/img/custle.png" alt="" style="display: none;">
                    <div class="img" style="background-image: url('<?= HOME_URL ?>/assets/img/custle.png')"></div>
                </div>
            </div>
        </div>
        <div class="white-area"></div>
    </div>
</div>