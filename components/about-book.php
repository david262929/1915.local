<?php
//    $aboutBook = $pageData['aboutBook'] ?? null;
//    if(!empty($aboutBook)){
//    ?>
    <div data-module="about-book" data-color="black">
        <div class="book-container" style="background-image: url('<?= !empty($pageData['aboutBook']['bgImg']) ? HOME_URL.$pageData['aboutBook']['bgImg'] : null ?>');">
            <div class="overlay"></div>
            <div class="text-container">
                <h4 class="name"><?= $pageData['aboutBook']['name'] ?? null ?></h4>
                <h3 class="excerpt"><?= $pageData['aboutBook']['excerpt'] ?? null ?></h3>
                <span class="order-now" data-popup="true"><?= $pageData['aboutBook']['order-now'] ?? null ?></span>
            </div>
        </div>
    </div>
<?php
//    }
?>