<div data-module="map" data-navbar-affect-color="black">
    <div class="map withParallax" style="background-image: url('<?= HOME_URL ?>/assets/img/map.jpg')">

    </div>
    <div class="mobile-map">
        <img src="<?= HOME_URL ?>/assets/img/map.jpg" alt="" class="static-mobile">
    </div>
    <div class="map-popup popup" data-is-opened="false">
        <div class="img-container popup_content" >
            <img src="<?= HOME_URL ?>/assets/img/map.jpg" alt="" class="static" data-zoommer-src="<?= HOME_URL ?>/assets/img/map-big.jpg">
        </div>
        <div class="close-popup"></div>
    </div>
</div>