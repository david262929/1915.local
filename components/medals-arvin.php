<div data-module="medalsArvin" data-scroll-side="down" data-color="white">
    <div class="medals-container">
        <div class="sub-modals-container" id="sub-modals-container">
            <?php
            $medalsArvin = $pageData['medalsArvin'] ?? null;
            if(!empty($medalsArvin)){
                $orderNow = $pageData['medalsArvin']['order-now'] ?? null;
                $medals = $medalsArvin['medals'] ?? null;
                $popupType = $medalsArvin['popupType'] ?? null;
                if(!empty($medals)){

                    $medalsDefaults = $pageData['medals-default-data'] ?? [];

                    $weightName = $medalsDefaults['weightName'] ?? '';
                    $weightType = $medalsDefaults['weightType'] ?? '';

                    $priceDetails = $medalsDefaults['priceDetails'] ?? [];
                    $priceDetailsHtml = "<div class='titlePopup'>{$priceDetails['name']}</div><div class='valuePopup'>{$priceDetails['price']} {$priceDetails['type']}</div>";
                    $allPriceDetailsHtml = "<div class='titlePopup'>{$priceDetails['allPriceText']}</div><div class='valuePopup'>{$priceDetails['price']}</div>";

                    $medalsDefaultDetails = $medalsDefaults['details'] ?? null;
                    $popupType = $medalsArvin['popupType'] ?? null;
                    $defaultsText = $defaultsTextFirst = '';
                    if(!empty($medalsDefaultDetails)) {
                        $defaultsTextFirst = "<div class='detail'><div class='titlePopup'>{$medalsDefaultDetails[0]['title']}</div><div class='valuePopup'>{$medalsDefaultDetails[0]['value']}</div></div>";
                        unset($medalsDefaultDetails[0]);
                        foreach ($medalsDefaultDetails as $key => $detail) {
                            $active = $key == 0 ? 'data-is-active="true"' : null;
                            $defaultsText .= "<div class='detail'><div class='titlePopup'>{$detail['title']}</div><div class='valuePopup'>{$detail['value']}</div></div>";
                        }
                    }


                    foreach ($medals as $key => $medal){
                        $active = $key == 0 ? 'data-is-active="true"' : null;
                        $popupId = ($key+1).'-medal-popup';
                        ?>
                        <div class="medal" data-item="<?= $medal['key'] ?? null ?>">
                            <div class="text-container">
                                <div class="sub-text-container fixator" <?= $active ?> >
                                    <h3 class="title"><?= $medal['title'] ?? null ?></h3>
                                    <h4 class="description"><?= $medal['description'] ?? null ?></h4>
                                    <span class="order-now" data-target-popup="#<?= $popupId ?>" >  <?= $orderNow ?>  </span>
                                    <div class="popup" id="<?= $popupId ?>" data-is-opened="false">
                                        <div class="popup_content">
                                            <div class="sub_popup_content">
                                                <div class="about">
                                                    <div class="sub-about">
                                                        <h4 class="popup-main-title"><?= $medal['popupTitle'] ?? null ?></h4>
                                                        <h5 class="popup-main-type"><?= $popupType ?></h5>
                                                        <h5 class="count-from"><span class="count">1915</span>/1915</h5>
                                                        <h5 class="details">
                                                            <?= $defaultsTextFirst ?>
                                                            <div class='detail'><div class='titlePopup'><?= $weightName?></div><div class='valuePopup'><?= $medal['weight'].' '.$weightType ?></div></div>
                                                            <?= $defaultsText ?><div class="clearfix"></div>
                                                        </h5>
                                                        <h5 class="details price"><div class="detail"><?= $priceDetailsHtml ?? null ?></div><div class="clearfix"></div></h5>
                                                        <h5 class="details all-price"><div class="detail"><?= $allPriceDetailsHtml ?? null ?></div><div class="clearfix"></div></h5>
                                                        <div class="action-buttons">
                                                            <div class="preview_opener">Preview</div>
                                                            <div class="confirm"><?= $medalsDefaults['confirm'] ?? null ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="sub-confirm" data-opened-confirmer="false">
                                                        <form>
                                                            <input type="text" name="name" placeholder="">
                                                            <input type="email" name="email" placeholder="">
                                                            <input type="phone" name="phone" placeholder="">
                                                            <textarea type="text" name="message" placeholder=""></textarea>
                                                            <input type="number" name="count">
                                                        </form>
                                                        <div class="action-buttons">
                                                            <div class="close-confirm">Cancel</div>
                                                            <div class="apply-confirm">Order</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="preview" data-opened-preview="false">
                                                    <div class="popup-slider">
                                                        <ul class="popup-slides">
                                                            <li class="popup-slide sub_preview popup-zoommer" data-popup-zoommer-src="<?= !empty($medal['onlyMedalImgs'][1]) ? HOME_URL.$medal['onlyMedalImgs'][1] : null ?>"  data-popup-zoommer-class="popup-zoommer-element">
                                                                <span class="popup-zommer-icon"></span>
                                                                <div class="popup-zoommer-loading"></div>
                                                            </li>
                                                            <li class="popup-slide sub_preview popup-zoommer" data-popup-zoommer-src="<?= !empty($medal['onlyMedalImgs'][0]) ? HOME_URL.$medal['onlyMedalImgs'][0] : null ?>"  data-popup-zoommer-class="popup-zoommer-element">
                                                                <span class="popup-zommer-icon"></span>
                                                                <div class="popup-zoommer-loading"></div>
                                                            </li>
                                                        </ul>
                                                        <div class="controls">
                                                            <div class="to-left"></div>
                                                            <div class="delimeter"></div>
                                                            <div class="to-right"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="close-popup"></div>
                                        </div>
                                        <div class="close-area"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="medal-container">
                                <img src="<?= !empty($medal['img']) ? HOME_URL.$medal['img'] : null ?>" data-zoommer-src="<?= !empty($medal['bigImg']) ? HOME_URL.$medal['bigImg'] : null ?>" alt="<?= !empty($medal['alt']) ? $medal['alt'] : null ?>">
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            ?>
            <!--            <div id="line"></div>-->
        </div>
        <div class="controlls">

        </div>
    </div>

</div>