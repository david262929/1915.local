<?php
if(!defined('ABS_PATH') || empty($pageData)){ header("Location: 404.php");die();}
?>

<!DOCTYPE html>
<html lang="<?= LANG ?>" xml:lang="<?= LANG ?>"  xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>1915.life</title>
    <link rel="icon" href="<?= HOME_URL ?>/assets/img/favicon.png">
    <?php
    echo '<link rel="stylesheet" href="' . HOME_URL . '/assets/styles/css/main.css?v='.rand(111,12122).'">';
    ?>
    <meta http-equiv="Cache-control" content="public">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="<?= LANG ?>">

<?php

include_once ('components/navbar.php');
include_once ('components/medals-slider.php');

echo '<script src="' . HOME_URL . '/assets/js/build/app.min.js?v='.rand(111,12122).'"></script>';
?>
</body>
</html>