const path = require('path');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

let conf = [
	{
		name: 'jsdavooooooo',
		entry: './assets/js/index.js',
		output: {
			path: path.resolve(__dirname, './assets/js/build/'),
			filename: 'app.min.js'
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					exclude: /(node_modules)/,
					loader: 'babel-loader',
					query: {
						presets: ['es2015']
					}
				}
			]
		},
		stats: {
			colors: true
		},
		devtool: 'source-map'
	}
];

module.exports = conf;